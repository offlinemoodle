<?php
// Make a MySQL Connection
mysql_connect("localhost", "root", "baxter") or die(mysql_error());
mysql_select_db("moodle") or die(mysql_error());

// Retrieve data from the lesson "answers" table
$result = mysql_query("SELECT id, lessonid, pageid, jumpto, answer, response FROM mdl_lesson_answers")
or die(mysql_error());  

$fields_num = mysql_num_fields($result);

//echo "<div id='dbresult' style='display:none'>";
echo "<h1>Table: {$table}</h1>";
echo "<table id = 'mdl_lesson_answers' border='1'><tr>";
// printing table headers
for($i=0; $i<$fields_num; $i++)
{
    $field = mysql_fetch_field($result);
    echo "<td>{$field->name}</td>";
}
echo "</tr>\n";
// printing table rows
while($row = mysql_fetch_row($result))
{
    echo "<tr>";

    // $row is array... foreach( .. ) puts every element
    // of $row to $cell variable
    foreach($row as $cell)
        echo "<td>$cell</td>";

    echo "</tr>\n";
}
//echo "</div>";
mysql_free_result($result);

// Retrieve data from the lesson "pages" table
$result = mysql_query("SELECT id, nextpageid FROM mdl_lesson_pages")
or die(mysql_error());  

$fields_num = mysql_num_fields($result);

//echo "<div id='dbresult1' style='display:none'>";
echo "<h1>Table: {$table}</h1>";
echo "<table id = 'mdl_lesson_pages' border='1'><tr>";
// printing table headers
for($i=0; $i<$fields_num; $i++)
{
    $field = mysql_fetch_field($result);
    echo "<td>{$field->name}</td>";
}
echo "</tr>\n";
// printing table rows
while($row = mysql_fetch_row($result))
{
    echo "<tr>";

    // $row is array... foreach( .. ) puts every element
    // of $row to $cell variable
    foreach($row as $cell)
        echo "<td>$cell</td>";

    echo "</tr>\n";
}
//echo "</div>";
mysql_free_result($result);
?>
