var DB_NAME = "mdl_offline_db";

function ajaxget(id, page) {
    alert('ajaxget ' + id + ' ' + page);
    var pagecontent = fetchpage(id, page);
    document.write(pagecontent);
    document.close();
}

function ajxget(url, formname) {
    alert('ajxget: ' + url + ' ' + formname);
    var frm = document.getElementById(formname);
    var id = frm.id.value;
    var pageid = frm.pageid.value;
    var page = eval(pageid);
    if (frm.jumpto) {
        var jumpto = eval(frm.jumpto.value);
        if (jumpto == -1) {
           page = page + 1;
        } else if (jumpto > 0) {
           page = jumpto;
        }
    }
    alert('ajxget: formname ' + formname + ' id ' + id + ' pageid ' + pageid + ' jumpto ' + jumpto);
    // get nextpage
    var nextpage = getnextpage(page);
    if(	!frm.answerid) {
        ajaxget(id, nextpage);
    } else {
        ajxget2(formname, frm, id, pageid, page);
    }
}

function ajxget2(formname, frm, id, pageid, page) {
        var answervalue = 0;
        var answertxt = "";
        var responsetxt = "";
        var jumpto, nextpage;
        for (var i=0;i<frm.answerid.length;i++) {
            if(frm.answerid[i].checked) {
               answervalue = frm.answerid[i].value;
            }
        }
        // get info from db
        var db = google.gears.factory.create('beta.database', '1.0')
        db.open(DB_NAME);
        if(db) {
            // get answer info for this page
            var rs = db.execute( "select jumpto, answer, response from mdl_lesson_answers where id =" + answervalue);
            while (rs.isValidRow()) {
               jumpto = rs.field(0);
               answertxt = rs.field(1);
               responsetxt = rs.field(2);
               rs.next();
            }
            rs.close();
            db.close();
        } else {
            alert("db not found");
        }
        if(jumpto<0) {
            page = page + 1;
        } else if (jumpto > 0) {
            page = jumpto;
        }
        // get nextpage
        var nextpage = getnextpage(page);
        // now create a response and write to document
        var content = "</div><em>Your answer</em> : <p>";
        content = content + answertxt;
        content = content + "</p><div class='response ";
        if (jumpto == -1) { 
            content = content + "correct";
            page = nextpage;
        } else {
            content = content + "incorrect";
        }
        content = content + "'><p>"
        content = content + responsetxt;
        content = content + "</p></div>";
        content = content + "<p style='text-align:center' onclick='ajaxget(" +  id + ', ' + page + ' )' + "'>Continue</p>";
        var pagecontent = fetchpage(id,pageid);
        var txt = pagecontent;
        var start = txt.indexOf('<form id="answerform"');
        var end = txt.indexOf('</form>',start);
        var newtxt = txt.substr(0,start) + content + txt.substring(end+7,txt.length);
        document.write(newtxt);
        document.close();
}

function getnextpage(page) {
    // get info from db
    var nextpage = page;
    var db = google.gears.factory.create('beta.database', '1.0');
    db.open(DB_NAME);
    if(db) {
        var nextpage = page;
        rs = db.execute("select nextpageid from mdl_lesson_pages where id =" + page);
        if (rs.isValidRow()) {
           nextpage = rs.field(0);
        } else {
           alert('row ' + page + ' not found in mdl_lesson_pages');
        }
        rs.close();
        if(nextpage > 0) {
            rs = db.execute("select jumpto, answer, response from mdl_lesson_answers where pageid =" + nextpage);
            if (rs.isValidRow()) {
                if (!(rs.field(1).length > 0 || rs.field(2).length > 0)) {
                var nextpageid = rs.field(0);
                    if(nextpageid == -1) {
                        nextpage = nextpage + 1;
                    } else {
                        nextpage = nextpageid;
                    }
                } 
                alert('end of branch returning to: ' + nextpage);
             }
        rs.close();
        }
        db.close();
    } else {
        alert("db not found");
    }
    return(nextpage);
}

function fetchpage(id,page) {
        if (page > 0) {
            var target = "view.php?id=" + id + "&pageid=" + page
        } else {
            var target = "http://tony-desktop/moodle/course/view.php?id=" + id + "&pageid=1";
        }
        var req = new XMLHttpRequest();
        req.open("GET", target, false);
        req.send(null);
        if(req.status == 200) {
           return(req.responseText);
        }
}
