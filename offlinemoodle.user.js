// ==UserScript==
// @name           offlinemoodle
// @namespace      http://tony-desktop/moodle
// @description    test greasemonkey script
// @include        http://tony-desktop/moodle/*
// ==/UserScript==
var allLinks, thisLink, count, temp, n, insert;

//document.body.setAttribute("onload", "init()");

function xfetch(xpath) {
    var rslt = document.evaluate(
        xpath,
        document,
        null,
        XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE,
        null);
    return rslt
}

// case 1 image resource
allLinks = document.evaluate(
    '//div/img',
    document,
    null,
    XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE,
    null);
//alert(allLinks.snapshotLength)
count = 0
for (var i = 0; i < allLinks.snapshotLength; i++) {
    thisLink = allLinks.snapshotItem(i);
    // do something with thisLink
    var alt = thisLink.getAttribute('alt')
    var altstr = 'the resource was not found'
    thisLink.alt = altstr
    var t = thisLink.getAttribute('src')
    count = count + 1
    var teststr = 'moodle/file.php'
    n = t.indexOf(teststr)
    if (n > -1) {
    temp = teststr + ' found at ' + n + ' ' +  t
    alert('case 1 ' + temp)
    var newstr = 'http://tony-desktop/moodle/moodledata' + t.substr(n+15)
    alert(newstr)
    thisLink.src = newstr
    }
}

// case 2 office document
allLinks = document.evaluate(
    '//frameset/frame',
    document,
    null,
    XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE,
    null);
//alert(allLinks.snapshotLength)
count = 0
for (var i = 0; i < allLinks.snapshotLength; i++) {
    thisLink = allLinks.snapshotItem(i);
    // do something with thisLink
    var t = thisLink.getAttribute('src')
    count = count + 1
    var teststr = 'moodle/file.php'
    n = t.indexOf(teststr)
    if (n > -1) {
    temp = teststr + ' found at ' + n + ' ' +  t
    alert('case 2 ' + temp)
    var newstr = 'http://tony-desktop/moodle/moodledata' + t.substr(n+15)
    alert(newstr)
    thisLink.src = newstr
    }
}

// case 3 pdf document
allLinks = document.evaluate(
    '//object',
    document,
    null,
    XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE,
    null);
//alert(allLinks.snapshotLength)
count = 0
for (var i = 0; i < allLinks.snapshotLength; i++) {
    thisLink = allLinks.snapshotItem(i);
    // do something with thisLink
    var t = thisLink.getAttribute('data')
    count = count + 1
    var teststr = 'moodle/file.php'
    n = t.indexOf(teststr)
    if (n > -1) {
    temp = teststr + ' found at ' + n + ' ' +  t
    alert('case 3 ' + temp)
    var newstr = 'http://tony-desktop/moodle/moodledata' + t.substr(n+15)
    alert(newstr)
    thisLink.href = newstr
    }
}

// case 4 pdf document
allLinks = document.evaluate(
    '//a',
    document,
    null,
    XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE,
    null);
//alert(allLinks.snapshotLength)
count = 0
for (var i = 0; i < allLinks.snapshotLength; i++) {
    thisLink = allLinks.snapshotItem(i);
    // do something with thisLink
    var t = thisLink.getAttribute('href')
    if (t) {
        count = count + 1
        var teststr = 'moodle/file.php'
        n = t.indexOf(teststr)
        if (n > -1) {
            temp = teststr + ' found at ' + n + ' ' +  t
            // alert('case 4 ' + temp)
            var newstr = 'http://tony-desktop/moodle/moodledata' + t.substr(n+15)
            // alert(newstr)
            thisLink.href = newstr
        }
    }
}

// case 5  file in directory
allLinks = document.evaluate(
    '//a',
    document,
    null,
    XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE,
    null);
// alert('case 5 ' + allLinks.snapshotLength)
count = 0
for (var i = 0; i < allLinks.snapshotLength; i++) {
    thisLink = allLinks.snapshotItem(i);
    // do something with thisLink
    var t = thisLink.getAttribute('onclick')
    if (t) {
        count = count + 1
        var teststr1 = 'this.target'
        n = t.indexOf(teststr1)
        if (n > -1) {
            var teststr = 'file.php'
            n = t.indexOf(teststr)
            if (n > -1) {
                temp = teststr + ' found at ' + n
                var newstr = t.substr(0, n-13) + ' popup("http://tony-desktop/moodle/moodledata' + t.substr(n+8) + '"'
                alert('case 5:' + temp + '\n' + t + '\n' + newstr + '\n'  )
                thisLink = newstr
            }
        }
    }
}

// case 6 form
var allforms = xfetch("//form");
for (var i = 0; i < allforms.snapshotLength; i++) {
    // do something with thisLink
    var form = allforms.snapshotItem(i);
    if (form.id == 'pageform' || form.id.substr(0,10) == 'answerform') {
      var url = form.getAttribute('action');
      url = url.replace('lesson.php', 'view.php');
      form.setAttribute('action', "");
      form.setAttribute('name', form.id);
      var clickstr = "ajxget('" + url + "','" + form.id + "')";
    }
    //alert(clickstr);
    // Loop through all the form fields
    for (var j = 0; j < form.elements.length; j++) {
        if (form.elements[j].type == 'submit') {
              form.elements[j].setAttribute("onclick",clickstr);
        }
    }
    //alert(form.parentNode.innerHTML);
}
