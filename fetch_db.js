
function fetchDb(){
	// Create a function that will receive data sent from the server
        ajaxRequest = new XMLHttpRequest();
	ajaxRequest.onreadystatechange = function(){
		if(ajaxRequest.readyState == 4){
			document.getElementById("dbresult").innerHTML = ajaxRequest.responseText;
                        alert('fetch done');
		}
	}
	var queryString = "?SELECT id, lessonid, pageid, jumpto, answer, response FROM mdl_lesson_answers";
	ajaxRequest.open("GET", "fetch_db.php" + queryString, true);
	ajaxRequest.send(null); 
}
function cacheDb(){
	// Create a function that will copy data sent from the server to a gears db
        // Datbase Setup
        var cell = new Array(6)
        db.open(DB_NAME);
        db.execute('drop table if exists mdl_lesson_answers');
        db.execute('create table if not exists mdl_lesson_answers ' + '(id int, lessonid int, pageid int, jumpto int, answer text, response text)');
        var mytbl = document.getElementById('mdl_lesson_answers')
        if (mytbl) {
            for (var i = 1; i < document.getElementById('mdl_lesson_answers').rows.length; i++) {
               for (var j = 0; j < document.getElementById('mdl_lesson_answers').rows[i].cells.length; j++) {
                   cell[j] = document.getElementById('mdl_lesson_answers').rows[i].cells[j].innerHTML;
               }
               db.execute('insert into mdl_lesson_answers values (?,  ?, ?, ?, ?, ?)', [cell[0], cell[1], cell[2], cell[3], cell[4], cell[5]]);
            }
        }
        db.execute('drop table if exists mdl_lesson_pages');
        db.execute('create table if not exists mdl_lesson_pages ' + '(id int, nextpageid int)');
        var mytbl = document.getElementById('mdl_lesson_pages')
        if (mytbl) {
            for (var i = 1; i < document.getElementById('mdl_lesson_pages').rows.length; i++) {
               for (var j = 0; j < document.getElementById('mdl_lesson_pages').rows[i].cells.length; j++) {
                   cell[j] = document.getElementById('mdl_lesson_pages').rows[i].cells[j].innerHTML;
               }
               db.execute('insert into mdl_lesson_pages values (?,  ?)', [cell[0], cell[1]]);
            }
        }
        alert('cache done: ' + db.lastInsertRowId);
        db.close();
}

function displayDb() {
        db.open(DB_NAME);
        var rslt = "<table id = 'mdl_lesson_answers_display' border = 2>";
        var rw = "";
        rw = "<tr>";
        rs = db.execute( 'select id, lessonid, pageid, jumpto, answer, response from mdl_lesson_answers');
        for (var i=0; i<rs.fieldCount();i++)
        {
            rw = rw + "<td>" + rs.fieldName(i) + "</td>";
        }
        rslt = rslt + rw + "</tr>";
        while (rs.isValidRow()) 
        {
            rw = "<tr>";
            for (var i=0; i<rs.fieldCount();i++) 
            {
                var rw = rw + "<td>" + rs.field(i) + "</td>";
            }
            rslt = rslt + rw + "</tr>";
            rs.next();
        }
        rslt = rslt + "</table>";

        // display mdl_lessons_pages
        rslt = rslt + "<table id = 'mdl_lesson_pages_display' border = 2>";
        rw = "<tr>";
        rs = db.execute( 'select id, nextpageid from mdl_lesson_pages');
        for (var i=0; i<rs.fieldCount();i++)
        {
            rw = rw + "<td>" + rs.fieldName(i) + "</td>";
        }
        rslt = rslt + rw + "</tr>";
        while (rs.isValidRow()) 
        {
            rw = "<tr>";
            for (var i=0; i<rs.fieldCount();i++) 
            {
                var rw = rw + "<td>" + rs.field(i) + "</td>";
            }
            rslt = rslt + rw + "</tr>";
            rs.next();
        }
        rslt = rslt + "</table>";
        document.getElementById("dbdisplay").innerHTML = rslt;
        rs.close();
        db.close();
}
