OUTPUT = ['../', 'testsite1']
PREFIX = [
'{',
'  "betaManifestVersion": 1,',
'  "version": "version 1.0",',
'  "entries": ['
]

SUFFIX = [
'   ]',
'}'
]

try:
    import sys
    import os
    import path
    import urllib2
    from BeautifulSoup import BeautifulSoup
except:
    print 'import failure'

#open manifest.json in OUTPUT for write
temp = path.path(OUTPUT[0])
output = temp.joinpath(OUTPUT[1])
absout = os.path.abspath(output)
print 'absout', absout
manifestname = output.joinpath('manifest.json')
manifest = open(manifestname, 'w')
for outlines in PREFIX:
    manifest.write(outlines)
    manifest.write('\n')
infile = open('filelist.txt')
filelist = infile.readlines()
count = 1
for filename in filelist:
    page = urllib2.urlopen(filename)
    soup = BeautifulSoup(page)
    temp = soup.prettify()
    temp1 = temp.replace('notloggedin', '')
    pretty = temp1.replace('moodle', 'testsite1')
    n = filename.find('moodle')
    page = path.path(filename[n+7:-1])
    print 'page', page
    fldrs = page.splitall()
    print 'fldrs =', fldrs
    for i in range(1, len(fldrs) - 1):
        if not os.path.isdir(fldrs[i]):
            print 'mkdir', fldrs[i]
            os.mkdir(fldrs[i])
        os.chdir(fldrs[i])
    os.chdir(absout)
    temp = page.replace('.php', '')
    realpage = temp.replace('?id=','') + '.html'
    print 'page', page, 'realpage', realpage
    try:
       outfile = open(realpage, 'w')
       print 'realpage', realpage
       outfile.write(pretty)
       outfile.close()
    except:
       print 'output error'
    s = '        { "url": "' + realpage + '" }'
    manifest.write(s)
    if count < len(filelist):
        manifest.write(',\n')
    else:
        manifest.write('\n')
    count += 1
for outlines in SUFFIX:
    manifest.write(outlines)
    manifest.write('\n')
manifest.close()
sys.exit()
